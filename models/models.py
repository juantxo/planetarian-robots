from abc import abstractmethod
from typing import Tuple
from enum import Enum
from uuid import uuid4

from exceptions import ValidationError



MAX_MARS_COORDINATE = 50


class Orientation(Enum):
    NORTH = "N"
    SOUTH = "S"
    EAST = "E"
    WEAST = "W"


class Move(Enum):
    """
    Represents the base move that a Robot can make on the
    playground
    """
    LEFT = "L"
    RIGHT = "R"
    FORWARD = "F"

    @classmethod
    def left(cls):
        print("Pointing Left")

    @classmethod
    def right(cls):
        print("Pointing Right")

    @classmethod
    def forward(cls):
        print("Moving Forward")


class Planet():
    """
    Base Planet class, can be extended to different
    planets with its specific rules
    """

    @abstractmethod
    def calculate_position(
        self,
        instruction,
        curr_x_pos,
        curr_y_pos
    ) -> Tuple[int, int, bool]:
        print("Implement in concrete class")


class MarsPlanet(Planet):
    """
    Mars Planet Class
    """

    def __init__(
        self,
        top_x_coord: int = MAX_MARS_COORDINATE,
        top_y_coord: int = MAX_MARS_COORDINATE
    ) -> None:
        """
        Initialize Mars Planet

        Args:
            top_x_coord (int): x-axis size of the Planet
            top_y_coord (int): y-axis size of the Planet

        Returns:
            None
        """
        super().__init__()
        self.name = "MARS"
        self.x_bound = top_x_coord - 1
        self.y_bound = top_y_coord - 1
        self.x_top_visited_boundaries = [False for x in range(top_x_coord)]
        self.x_bottom_visited_boundaries = [False for x in range(top_x_coord)]
        self.y_left_visited_boundaries = [False for y in range(top_y_coord)]
        self.y_right_visited_boundaries = [False for y in range(top_y_coord)]
        # self.board = [["-" for y in range(top_y_coord+2)] for x in range(top_x_coord+2)]

    def calculate_position(
        self,
        curr_x_pos,
        curr_y_pos,
        orientation
    ) -> Tuple[int, int, bool]:
        """
        Validate if the Robot can make the move

        Args:
            instruction (Move):
            curr_x_pos (int):
            curr_y_pos (int):
            orientation (Orientation):

        Returns:
            Tuple[int, int, bool]
        """
        #self.board[curr_x_pos][curr_y_pos] = "-"
        movement_map = {
            Orientation.NORTH: (0, 1),
            Orientation.EAST: (1, 0),
            Orientation.SOUTH: (0, -1),
            Orientation.WEAST: (-1, 0)
        }
        add_x, add_y = movement_map[orientation]
        curr_x_pos += add_x
        curr_y_pos += add_y
        robot_active = True
        if not 0 <= curr_x_pos <= self.x_bound:
            if orientation == Orientation.EAST:
                if not self.y_right_visited_boundaries[curr_y_pos]:
                    self.y_right_visited_boundaries[curr_y_pos] = True
                    robot_active = False
                else:
                    print("You can't move there")
            elif orientation == Orientation.WEAST:
                if not self.y_left_visited_boundaries[curr_y_pos]:
                    self.y_left_visited_boundaries[curr_y_pos] = True
                    robot_active = False
                else:
                    print("You can't move there")

        elif not 0 <= curr_y_pos <= self.y_bound:
            if orientation == Orientation.NORTH:
                if not self.x_top_visited_boundaries[curr_x_pos]:
                    self.x_top_visited_boundaries[curr_x_pos] = True
                    robot_active = False
                else:
                    print("You can't move there")
            elif orientation == Orientation.SOUTH:
                if not self.x_bottom_visited_boundaries[curr_x_pos]:
                    self.x_bottom_visited_boundaries[curr_x_pos] = True
                    robot_active = False
                else:
                    print("You can't move there")

        return curr_x_pos, curr_y_pos, robot_active


class Robot():
    """
    Robot Class
    """
    MOVE_MAP = {
        "R": Move.RIGHT,
        "L": Move.LEFT,
        "F": Move.FORWARD
    }
    ORIENTATION_MAP = {
        "N": Orientation.NORTH,
        "S": Orientation.SOUTH,
        "W": Orientation.WEAST,
        "E": Orientation.EAST
    }
    ORIENTATION_MOVE_MAP = {
        Move.RIGHT: {
            Orientation.NORTH: Orientation.EAST,
            Orientation.EAST: Orientation.SOUTH,
            Orientation.SOUTH: Orientation.WEAST,
            Orientation.WEAST: Orientation.NORTH
        },
        Move.LEFT: {
            Orientation.NORTH: Orientation.WEAST,
            Orientation.EAST: Orientation.NORTH,
            Orientation.SOUTH: Orientation.EAST,
            Orientation.WEAST: Orientation.SOUTH
        }
    }

    def __init__(
        self,
        planet: MarsPlanet,
        name: str = uuid4(),
        x_pos: int = 0,
        y_pos: int = 0,
        orientation: str = "N"  # Orientation = Orientation.NORTH
    ) -> None:
        """
        Initialize Robot related to a specific Planet, if the Robot is created
        withouth argument it will create one in the 0,0 coordinates with NORTH
        orientation

        Args:
            name (str): Robots name
            planet (MarsPlanet): Planet where the robot was deployed
            x_pos (int): x-axis base position on the planet
            y_pos (int): y-axis base position on the planet
            orientation (Orientation): [default=Orientation.NORTH]

        Returns:
            None
        """
        if 0 <= x_pos <= planet.x_bound and 0 <= y_pos <= planet.y_bound:
            self.name = name
            self.active = True
            self.x_pos = x_pos
            self.y_pos = y_pos
            self.curr_x_pos = x_pos
            self.curr_y_pos = y_pos
            self.orientation = self.ORIENTATION_MAP[orientation]
            self.planet = planet
        else:
            raise ValidationError(error="You can't initialize a Robot outside the Planet limits")

    @property
    def current_position(self):
        lost = " LOST" if not self.active else ""
        return f"{self.curr_x_pos} {self.curr_y_pos} {self.orientation._value_}{lost}"

    def _update_orientation(self, instruction) -> bool:
        """
        Calculates the new orientation of the Robot

        Args:
            instruction (Move): new orientation

        Returns:
            bool: whether the orientation was updated or not
        """
        try:
            self.orientation = self.ORIENTATION_MOVE_MAP[instruction][self.orientation]
            return True
        except KeyError:
            print("Error while updating Robot orientation")
            return False

    def move(self, instruction: str) -> None:
        """
        Move the Robot inside the Planet.

        Args:
            instruction (str): instruction to perform

        Returns:
            None
        """
        instruction = self.MOVE_MAP[instruction]
        if self.active:
            if instruction == Move.FORWARD:
                self.curr_x_pos, self.curr_y_pos, self.active = self.planet.calculate_position(self.curr_x_pos,
                                                                                            self.curr_y_pos,
                                                                                            self.orientation)
                if not self.active:
                    print(f"LOST at (X,Y): ({self.curr_x_pos}, {self.curr_y_pos})")
                    return
            else:
                self._update_orientation(instruction)

            print(f"X Coordinate: {self.curr_x_pos}.\nY Coordinate: {self.curr_y_pos}.\nOrientation: {self.orientation}\n")
            

        else:
            print("The Robot is not longer operable")
