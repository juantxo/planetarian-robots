import pytest

from models.models import Robot, MarsPlanet, Orientation

# read from stdin the initialization args
# robot = models.Robot(name="R2D2",
#                      planet=planet,
#                      x_pos=x_pos,
#                      y_pos=y_pos,
#                      orientation="N")
    
@pytest.fixture()
def robot():
    planet = MarsPlanet()
    return Robot(name="R2D2", planet=planet)


MOVEMENT_PARAMS = (
    # instruction, expected_result
    ("F", Orientation.NORTH),
    ("L", Orientation.WEAST),
    ("R", Orientation.EAST)
)

@pytest.mark.parametrize("instruction, expected_result", MOVEMENT_PARAMS)
def test_move_forward(robot, instruction, expected_result):
    robot.move(instruction)
    assert robot.orientation == expected_result
