import pytest
from main import init_mars_planet

from models.models import MarsPlanet, Robot
from exceptions import ValidationError



MAIN_TEST_PARAMS = (
    # planet_values, (robot_1_origin, "robot_1_instructions, robot_1_result"), (robot_2_values), (robot_3_values)
    (
        "5 3",
        ("1 1 E", "RFRFRFRF", "1 1 E"),
        ("3 2 N", "FRRFLLFFRRFLL", "3 3 N LOST"),
        ("0 3 W", "LLFFFLFLFL", "2 3 S"),
        ("3 2 N", "FRRFLLFFRRFLL", "3 3 S LOST"),
    ),
)

@pytest.mark.parametrize("planet_values, r1_args, r2_args, r3_args, r4_args", MAIN_TEST_PARAMS)
def test_main_planet(capfd, planet_values, r1_args, r2_args, r3_args, r4_args):
    p_x, p_y = map(int, planet_values.split(" "))
    planet = MarsPlanet(p_x, p_y)

    r1_init, r1_cmd, r1_res = r1_args
    r1_x, r1_y, r1_o = r1_init.split(" ")
    r1 = Robot(planet=planet,
               x_pos=int(r1_x),
               y_pos=int(r1_y),
               orientation=r1_o)

    for cmd in r1_cmd:
        r1.move(cmd)

    assert r1.current_position == r1_res


    r2_init, r2_cmd, r2_res = r2_args
    r2_x, r2_y, r2_o = r2_init.split(" ")
    r2 = Robot(planet=planet,
               x_pos=int(r2_x),
               y_pos=int(r2_y),
               orientation=r2_o)

    for cmd in r2_cmd:
        r2.move(cmd)

    assert r2.current_position == r2_res

    r3_init, r3_cmd, r3_res = r3_args
    r3_x, r3_y, r3_o = r3_init.split(" ")

    with pytest.raises(ValidationError) as e:
        r3 = Robot(planet=planet,
                x_pos=int(r3_x),
                y_pos=int(r3_y),
                orientation=r3_o)


    r4_init, r4_cmd, r4_res = r4_args
    r4_x, r4_y, r4_o = r4_init.split(" ")
    r4 = Robot(planet=planet,
               x_pos=int(r4_x),
               y_pos=int(r4_y),
               orientation=r4_o)

    for cmd in r4_cmd:
        r4.move(cmd)

    assert r4.current_position == r4_res
