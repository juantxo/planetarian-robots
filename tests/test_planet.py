import pytest

from models.models import MarsPlanet, Move, Orientation, Robot


@pytest.fixture()
def planet():
    planet = MarsPlanet(top_x_coord=6, top_y_coord=6)

    return planet


# curr_x_pos, curr_y_pos, orientation, is_active_after_op, expected_new_x_pos, expected_new_y_pos
MOVEMENTS_PARAMS = (
    (4, 4, Orientation.NORTH, True, 4, 5),
    (4, 3, Orientation.NORTH, True, 4, 4),
    (4, 4, Orientation.SOUTH, True, 4, 3),
)

@pytest.mark.parametrize(
    "curr_x_pos, curr_y_pos, orientation, is_active_after_op, expected_new_x_pos, expected_new_y_pos",
    MOVEMENTS_PARAMS
)
def test_calculate_position(
    planet,
    curr_x_pos,
    curr_y_pos,
    orientation,
    is_active_after_op,
    expected_new_x_pos,
    expected_new_y_pos
):
    x_pos, y_pos, active = planet.calculate_position(curr_x_pos, curr_y_pos, orientation)
    assert x_pos == expected_new_x_pos
    assert y_pos == expected_new_y_pos
    assert active == is_active_after_op


def test_move_outside_planet_boundaries(planet):
    instruction = "F"
    r1 = Robot(planet=planet,
               x_pos=4,
               y_pos=4,
               orientation=Orientation.NORTH._value_)
    r2 = Robot(planet=planet,
               x_pos=4,
               y_pos=4,
               orientation=Orientation.NORTH._value_)

    r1.move(instruction)
    assert r1.active
    r1.move(instruction)
    assert not r1.active

    r2.move(instruction)
    assert r2.active
    r2.move(instruction)
    assert r2.active

