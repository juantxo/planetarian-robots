

class ValidationError(Exception):
    
    def __init__(self, error) -> None:
        super().__init__()
        
        self.error = error