from uuid import uuid4
from models import models


def create_robot(planet):
    # read from stdin the initialization args
    print("Enter Robot origin coordinates and orientation: ")
    x_pos, y_pos, orientation = input().split(" ")
    robot = models.Robot(planet=planet,
                         x_pos=int(x_pos),
                         y_pos=int(y_pos),
                         orientation=orientation)
    return robot

def init_mars_planet():
    print("Enter the Planet size: ")
    planet_input = input()
    top_x, top_y = planet_input.split(" ")
    planet = models.MarsPlanet(top_x_coord=int(top_x),
                               top_y_coord=int(top_y))

    r1 = create_robot(planet)
    
    robot_lst = [r1]
    current_robot = r1
    while True:
        print("\nEnter your next order:\n"\
              f"\t1) Enter next instruction (Active Robot: {current_robot.name})\n"\
              "\t2) Create new robot\n"\
              "\t3) Change active Robot\n"\
              "\t4) List active Robots\n"\
              "\t5) Check Robot status\n"\
              "\t6) Planet configs\n"\
              "\t7) Exit\n")

        cmd = int(input())
        if cmd == 1:
            instruction = input("Instruction: ")
            for command in instruction:
                current_robot.move(command)
        if cmd == 2:
            robot_lst.append(create_robot(planet))
        if cmd == 3:
            robot_number = input("Enter the Robot position: ")
            current_robot = robot_lst[int(robot_number)]
        if cmd == 4:
            print("Active Robots:\n")
            for idx, robot in enumerate(robot_lst):
                print(f"\tRobot name: {robot.name}. Index: {idx}")
        if cmd == 5:
            idx = robot_lst.index(current_robot)
            print(f"Robot name: {current_robot.name}.\nList Position: {idx}\n"\
                  f"Robot status: {current_robot.active}\nCurrent Planet: {current_robot.planet.name}\n"\
                  f"Current position: {current_robot.curr_x_pos}, {current_robot.curr_y_pos}\n")
        if cmd == 6:
            print(f"Planet name: {planet.name}\nPlanet X Boundary: {planet.x_bound}\n"\
                  f"Planet Y Boundary: {planet.y_bound}\n"\
                  f"Visited Top x-axis limits: {planet.x_top_visited_boundaries}\n"\
                  f"Visited Bottom x-axis limits: {planet.x_bottom_visited_boundaries}\n"\
                  f"Visited Bottom y-axis limits: {planet.y_top_visited_boundaries}\n"\
                  f"Visited Bottom y-axis limits: {planet.y_bottom_visited_boundaries}\n")
        if cmd == 7:
            break




if __name__ == "__main__":
    init_mars_planet()